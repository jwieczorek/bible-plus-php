<?php namespace JWIECZOREK;
/*
File: json_parser.php
Path: bible-plus/jwieczorek/json_parser.php
Created: 12.22.16 @ 13:15 EST
Modified: 01.19.17 @ 17:57 EST
Author: Joshua Wieczorek
---
Description: Contains plugin's template bible class.
This class gets and returns the html templates for
the json.
*/

class Json_Parser
{
    /**
     * Cleans json response
     */
    public function parse($json='')
    {
        // Create array out of json
        $array  = json_decode($json, 1);

        // If valid response type
        if(!isset($array['type'])) :
            return null;
        endif;

        // Switch resonse type
        switch( $array['type'] )
        {
            case 'book':
                // Clean for book
                $new_array = $this->_clean_books($array);
                break;
            case 'chapter':
                // Clean for chapter
                $new_array = $this->_clean_chapters($array);
                break;
            case 'verse':
                // Clean for verse
                $new_array = $this->_clean_verses($array);
                break;
        }
        // Return json
        return json_encode($new_array);
    }

    /**
     * Clean JSON for book
     */
    private function _clean_books($array=array())
    {
        // Iniate the book array
        $b_array = array();
        // Loop through the book's chapters
        foreach($array['book'] as $ch_num => $data) :
            // Set the chapter number to the verse array
            $b_array[$ch_num] = $this->_extract_verses($data['chapter']);
        endforeach;
        // Return the book array
        return $b_array;
    }

    /**
     * Clean JSON for chapter
     */
    private function _clean_chapters($array=array())
    {
        // Initiate the chapter array
        $c_array = array();
        // Set the chapter number and assign the verses
        $c_array[$array['chapter_nr']] = $this->_extract_verses($array['chapter']);
        // Return the chapter
        return $c_array;
    }

    /**
     * Clean JSON for verse
     */
    private function _clean_verses($array=array())
    {
        // Iniate the book array
        $v_array = array();
        // Loop through the book's chapters
        foreach($array['book'] as $data) :
            // Set the chapter number to the verse array
            $v_array[$data['chapter_nr']] = $this->_extract_verses($data['chapter']);
        endforeach;
        // Return the book array
        return $v_array;
    }

    /**
     * Extract verses
     */
     private function _extract_verses($array=array())
     {
         // Initiate verse array varialbe
         $v_array = array();
         // Loop through verses
         foreach($array as $key => $data) :
             // Set the verse number to the verse text
             $v_array[$key] = $data['verse'];
         endforeach;
         // Return the verse array
         return $v_array;
     }
}
