<?php namespace JWIECZOREK;
/*
File: bible.php
Path: bible-plus/jwieczorek/bible.php
Created: 12.22.16 @ 13:15 EST
Modified: 01.19.17 @ 17:57 EST
Author: Joshua Wieczorek
---
Description: Contains plugin's bible class.
This class gets and returns the bible
passages in html.
*/

class Bible
{
    // HTTP client
    private $_http;
    // Template engine
    private $_template;
    // JSON parser
    private $_json_parser;
    // Bible api url
    private $_api_url;
    // Cache directory
    private $_cache_dir;
    // Set cache file extention
    private $_cache_ext;
    // Shortcode attributes
    private $_atts;
    // Bible passage
    private $_passage;
    // Bible version
    private $_version;
    // Css classes
    private $_css_classes;
    // No bible error
    private $_no_bible_error;

    /**
     * Sets defaults for many of this class'
     * variables.
     */
    public function __construct()
    {
        // Set http client
        $this->_http            = new \GuzzleHttp\Client(['verify' => false ]);
        // Set template engine
        $this->_template        = new Template();
        // Set JSON parser
        $this->_json_parser     = new Json_Parser();
        // Default api url
        $this->_api_url         = 'https://getbible.net/json?passage=';
        // Default cache directory
        $this->_cache_dir       = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'bible-plus-cache'.DIRECTORY_SEPARATOR;
        // Default cache file extention
        $this->_cache_ext       = 'bbp';
        // Shortcode attrubytes
        $this->_atts            = array();
        // Set passage by default
        $this->_passage         = 'Jn3:16';
        // Set version by default
        $this->_version         = 'KJV';
        // Set no bible error
        $this->_no_bible_error  = 'Something went wrong with the bible. Please make sure that you are requesting a valid passage! If this problem presits please contact support@bible-plus.org with error code #101!';
    }

    /**
     * Sets the cache file extention
     */
    public function set_cache_ext($ext='')
    {
        if( $ext != '' ) :
            $this->_cache_ext = $ext;
        endif;
    }

    /**
     * Sets the desired passage
     */
    public function set_passage($passage='')
    {
        if( $passage != '' ) :
            $this->_passage = str_replace(' ','',$passage);
        endif;
    }

    /**
     * Sets the desired version
     */
    public function set_version($version='')
    {
        if( $version != '' ) :
            $this->_version = str_replace(' ','',$version);
        endif;
    }

    /**
     * Render html to screen
     */
    public function render($passage='',$version='',$atts=array())
    {
        // Set passage
        $this->set_passage($passage);
        // Set version
        $this->set_version($version);
        // Set template attributes
        $this->_template->set_atts($atts);
        // Get json and extract into array
        if($bible = json_decode($this->_get_bible_json(), 1)) :
            // Return PHP template with array
            return $this->_template->render($bible);
        endif;
        // Else return error occurred
        return $this->_no_bible_error;

    }

    /**
     * Gets the bible
     */
    private function _get_bible_json()
    {
        // If cache exists
        if($this->_cache_exists() && $json=file_get_contents($this->_cache_path())) :
            // Return json
            return $json;
        // If no cache exists
        else :
            // Make the curl reuquest and get json
            $json = $this->_curl_request();
            // Clean json
            $clean_json = $this->_json_parser->parse($json);
            // If is valid json
            if($clean_json) :
                // Cache json
                $this->_cache_json($clean_json);
            endif;
            // Return json
            return $clean_json;
        endif;
    }

    /**
     * Create full api request url
     */
    private function _api_request_url()
    {
        // Return full api url
        return $this->_api_url.$this->_passage.'&v='.$this->_version;
    }

    /**
     * Create filename
     */
    private function _filename()
    {
        // Convert to lowercase
        $filename   = strtolower($this->_passage);
        // Replace empty spaces
        $filename   = str_replace(' ','',$filename);
        // Replace colons with underscores
        $filename   = str_replace(';','_',$filename);
        // Replace semicolon with period
        $filename   = str_replace(':','.',$filename);
        // Return filename
        return $filename;
    }

    /**
     * Create CURL request and return json from response
     */
    private function _curl_request()
    {
        // Curl response
        $response = $this->_http->request('GET',$this->_api_request_url());
        // Return response body trimmed
        return ($response->getStatusCode() == 200) ? trim($response->getBody(),'();') : null;
    }

    /**
     * Create cache directory
     */
    private function _create_cache_dir()
    {
        // If cache directory does not exists
        if(!file_exists($this->_cache_dir)) :
            // Create cache directory
            mkdir($this->_cache_dir, 0777, true);
        endif;
    }

    /**
     * Create full cache path
     */
    private function _cache_path()
    {
        // Return cache directory and filename
        return $this->_cache_dir.$this->_filename().'-'.$this->_version.'.'.$this->_cache_ext;
    }

    /**
     * See if cache file exists
     */
    private function _cache_exists()
    {
        // Return true if file exists otherwise false
        return (file_exists($this->_cache_path())) ? true : false;
    }

    /**
     * Cache json into file
     */
    private function _cache_json($json='')
    {
        // Crate file and put json in it
        file_put_contents($this->_cache_path(), $json);
    }
}
