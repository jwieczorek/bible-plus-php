<?php namespace JWIECZOREK;
/*
File: template.php
Path: bible-plus/jwieczorek/template.php
Created: 12.22.16 @ 13:15 EST
Modified: 01.19.17 @ 17:57 EST
Author: Joshua Wieczorek
---
Description: Contains plugin's template bible class.
This class gets and returns the html templates for
the json.
*/

class Template
{
    private $_css_classes;

    private $_atts;

    function __construct()
    {
        $this->_css_classes = $this->_set_css_classs();
    }

    public function set_atts($atts=array())
    {
        $this->_atts = array(
            'cnum'  => isset($atts['cnum']) ? $atts['cnum'] : 'yes',
            'vnum'  => isset($atts['vnum']) ? $atts['vnum'] : 'yes',
            'vpl'  => isset($atts['vpl']) ? $atts['vpl'] : 'yes'
        );
    }

    private function _set_css_classs()
    {
        // Css classes
        return array(
            'container'     => 'bible-plus-verse-container',
            'chapter-title' => 'bible-plus-chapter-title',
            'verse'         => 'bible-plus-verse',
            'verse-number'  => 'bible-plus-verse-number'
        );
    }

    /**
     * Render html php template
     */
     public function render($bible=array())
     {
         // Open html
         $html  = '<div class="'.$this->_css_classes['container'].'">';
         // PHP template
         $html .= $this->_php_template($bible);
         // Add attribution
         $html .= '<div class="bible-plus-powered-by" style="font-size:11px;"><a href="https://bible-plus.org" target="_blank"><em>Powered by Bible Plus+</em></a></div>';
         // Close html
         $html .= '</div>';
         // Return html
         return $html;
     }

    /**
     * Render html php template
     */
    private function _php_template($bible=array())
    {
        // Set verse html dom element
        $verse_element = ($this->_atts['vpl'] === 'yes') ? 'p' : 'span';
        // Open html
        $html = '';
        // Loop through bible chapters
        foreach($bible as $chapter => $verses) :
            // Show title if chapter numbers are on
            $html .= $this->_php_chapter_title($chapter);
            // Render verses
            $html .= $this->_php_render_verses($verses, $verse_element);
        // End loop through chapters
        endforeach;
        // Return html
        return $html;
    }

    /**
     * Render chapter title for php template
     */
    private function _php_chapter_title($chapter_number)
    {
        // If chapter numbers
        if( $this->_atts['cnum'] == 'yes' ) :
            return '<h4 class="'.$this->_css_classes['chapter-title'].'">Chapter '.$chapter_number.'</h4>' ;
        // End chapter numbers
        endif;
    }

    /**
     * Render verses for php template
     */
    private function _php_render_verses($verses, $ele)
    {
        // Instantiate html
        $html = '';
        // Loop through verses
        foreach( $verses as $number => $text ) :
            // Open html
            $html .= '<'.$ele.' class="'.$this->_css_classes['verse'].'">';
            // Show verse number
            $html .= $this->_php_verse_number($number);
            // Verse text
            $html .= $text;
            // Close html
            $html .= '</' . $ele . '>';
        // End verse loop
        endforeach;
        // Return html
        return $html;
    }

    /**
     * Render verse number for php template
     */
    private function _php_verse_number($verse_number)
    {
        // If verse numbers
        if( $this->_atts['vnum'] == 'yes' ) :
            return '<span class="'.$this->_css_classes['verse-number'].'"><small>'.$verse_number.'</small></span> ';
        // End verse numbers
        endif;
    }
}
