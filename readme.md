## Description

This is the core of the WordPress plugin [Bible Plus+](https://bible-plus.org).

With this PHP app you can simply display the Bible in over 20 languages in you PHP app.

*This app uses [GETBIBLE.net's](https://getbible.net/api) Bible api*

## Installation

Just include the autoload.php file in you app.

### Example

<?php

require 'bible-plus/autoload.php';

## How To Use

<?php

require 'bible-plus/autoload.php';

$bible = new \JWIECZOREK\Bible;

echo $bible->render('John 3:16','nasb');

### Advanced Usage

echo $bible->render('John 3:16','nasb' array('cnum'=>'no','vnum'=>'no','vpl'=>'no'));

- **cnum** = show chapter number | yes/no | default:yes
- **vnum** = show verse number | yes/no | default:yes
- **vpl** = show verses as one-verse-per-line | yes/no | default:yes
